# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView


class BannerView(BrowserView):
    """ Return fields for content type Banner
    """

    def get_data(self):
        D={}
        obj = self.context
        D['title'] = obj.Title()
        D['description'] = obj.Description()
        D['link'] = obj.link
        D['target'] = obj.target
        D['url'] = obj.absolute_url()
        return D