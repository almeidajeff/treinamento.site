# -*- coding: utf-8 -*-

from pkg_resources import resource_stream
from plone.app.testing import SITE_OWNER_NAME
from plone.app.testing import SITE_OWNER_PASSWORD
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from plone.testing.z2 import Browser
from treinamento.site.testing import TREINAMENTO_SITE_FUNCTIONAL_TESTING
from treinamento.site.testing import TREINAMENTO_SITE_INTEGRATION_TESTING
from zope.component import createObject
from zope.component import queryUtility
import unittest


class BannerIntegrationTest(unittest.TestCase):

    layer = TREINAMENTO_SITE_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='banner')
        self.assertTrue(fti)

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='banner')
        schema = fti.lookupSchema()
        self.assertTrue(schema)
        # self.assertEqual(IBanner, schema)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='banner')
        factory = fti.factory
        banner = createObject(factory)
        # self.assertTrue(IBanner.providedBy(banner))
        self.assertTrue(banner)

    def test_adding(self):
        self.portal.invokeFactory('banner', 'banner')
        self.assertTrue(self.portal.banner)
        # self.assertTrue(IBanner.providedBy(self.portal.banner))


class BannerFunctionalTest(unittest.TestCase):

    layer = TREINAMENTO_SITE_FUNCTIONAL_TESTING

    def setUp(self):
        app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.portal_url = self.portal.absolute_url()

        # Set up browser
        self.browser = Browser(app)
        self.browser.handleErrors = False
        self.browser.addHeader(
            'Authorization',
            'Basic %s:%s' % (SITE_OWNER_NAME, SITE_OWNER_PASSWORD,)
        )

    def test_add_banner(self):
        self.browser.open(self.portal_url + '/++add++banner')
        ctrl = self.browser.getControl
        ctrl(name="form.widgets.IDublinCore.title").value = "My banner"
        ctrl(name="form.widgets.IDublinCore.description").value = \
            "This is my banner"
        ctrl(name="form.widgets.link").value = "http://www.plone.org"
        ctrl(name="form.widgets.target").value = "Sim"
        img_ctrl = ctrl(name="form.widgets.image")
        img_ctrl.add_file(resource_stream(__name__, 'plone.png'),
                          'image/png', 'plone.png')
        ctrl("Save").click()

        banner = self.portal['my-banner']

        self.assertEqual('My banner', banner.title)
        self.assertEqual('This is my banner',  banner.description)
        self.assertEqual('http://www.plone.org', banner.link)
        self.assertEqual('Sim', banner.target)
        self.assertEqual((491, 128), banner.image.getImageSize())

    def test_view_banner(self):
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "banner",
            id="my-banner",
            title="My banner",
        )

        import transaction
        transaction.commit()

        self.browser.open(self.portal_url + '/my-banner')

        self.assertTrue('My banner' in self.browser.contents)

    def test_custom_template(self):
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "banner",
            id="my-banner",
            title="My banner",
        )

        import transaction
        transaction.commit()

        self.browser.open(self.portal_url + '/banner-view')

        self.assertIn('Dexterity is the new default', self.browser.contents)
        self.assertIn(
            'Diazo is a powerful tool for theming', self.browser.contents)
        self.assertIn('Magic templates in Plone 5', self.browser.contents)
        self.assertIn('The State of Plone', self.browser.contents)