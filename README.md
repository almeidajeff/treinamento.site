# treinamento.site

.. This README is meant for consumption by humans and pypi. Pypi can render rst files so please do not use Sphinx features.
   If you want to learn more about writing documentation, please check out: http://docs.plone.org/about/documentation_styleguide.html
   This text does not appear on pypi or github. It is a comment.

==============================================================================
treinamento.site
==============================================================================

Tell me what your product does

Features
--------

- Can be bullet points


Examples
--------

This add-on can be seen in action at the following sites:
- Is there a page on the internet where everybody can see the features?


Documentation
-------------

Full documentation for end users can be found in the "docs" folder, and is also available online at http://docs.plone.org/foo/bar


Translations
------------

This product has been translated into

- Jeff Almeida


Installation
------------

Install ploneconf.site by adding it to your buildout::

    [buildout]

    ...

    eggs =
        treinamento.site


and then running ``bin/buildout``


Contribute
----------

- Issue Tracker: https://gitlab.com/almeidajeff/treinamento.site/issues
- Source Code: https://gitlab.com/almeidajeff/treinamento.site
- Documentation: https://gitlab.com/almeidajeff/treinamento.site/docs


Support
-------

If you are having issues, please let us know.
We have a mailing list located at: jralmeida88@gmail.com


License
-------

The project is licensed under the GPLv2.